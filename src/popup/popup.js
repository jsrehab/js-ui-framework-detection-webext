/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

const getCurrentTab = () => {
  return browser.tabs.query({
    active: true,
    windowId: browser.windows.WINDOW_ID_CURRENT,
  });
};

const getVersionText = (version) => {
  console.log(version);
  return version && (!Array.isArray(version) || version.length > 0)
    ? version
    : '?';
};

const getReportRow = (rowName, rowValue) => {
  const rowElement = document.createElement('tr');

  const nameDataElement = document.createElement('td');
  nameDataElement.textContent = rowName;
  rowElement.appendChild(nameDataElement);

  const valueDataElement = document.createElement('td');
  valueDataElement.textContent = rowValue;
  rowElement.appendChild(valueDataElement);

  return rowElement;
};

const updatePopupContent = (detectionData) => {
  console.log(detectionData);

  const tableBody = document.querySelector('table > tbody');

  // Remove the placeholder row
  Array.from(tableBody.children).at(-1).remove();

  tableBody.appendChild(getReportRow(
    'Bootstrap JS',
    getVersionText(detectionData.frameworkJSVersions.bootstrap),
  )).classList.add('table-js-version');

  tableBody.appendChild(getReportRow(
    'Bootstrap CSS',
    getVersionText(detectionData.frameworkCSSVersions.bootstrap),
  )).classList.add('table-css-version');

  detectionData.components.forEach((component) => {
    tableBody.appendChild(getReportRow(
      component.component,
      component.count,
    )).classList.add('table-component');
  });
};

const onMessageReceived = (message) => {
  getCurrentTab().then((tabs) => {
    const tabId = tabs[0].id;
    console.log(tabId, message);

    if (message.tabId === tabId && message.detectionData) {
      updatePopupContent(message.detectionData);
    }
  });
};

browser.runtime.onMessage.addListener(onMessageReceived);

browser.runtime.sendMessage({ popupOpened: true });
