/*
 * Copyright (c) 2022 Inria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

'use strict';

const getCurrentTab = () => {
  return browser.tabs.query({
    active: true,
    windowId: browser.windows.WINDOW_ID_CURRENT,
  });
};

const tabDetectionData = [];

const onMessageReceived = async (message, sender) => {
  const tabId = sender.tab ? sender.tab.id : (await getCurrentTab())[0].id;

  if (message.popupOpened) {
    console.log('Popup opened');

    const detectionData = tabDetectionData[tabId];
    console.log(tabId, detectionData, tabDetectionData);
    browser.runtime.sendMessage({ tabId, detectionData })
      .catch((err) => {
        console.error(err);
      });
  }

  if (message.inspectionData) {
    tabDetectionData[tabId] = message.detectionData;
    console.log(tabId, tabDetectionData);
  }
};

browser.runtime.onMessage.addListener(onMessageReceived);
