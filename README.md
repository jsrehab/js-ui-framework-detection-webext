# js-ui-framework-detection-webext
This repository contains the
[WebExtension](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions)
used by the crawl script (found in `crawl-bootstrap` in this repository group)
which detects the usage of Bootstrap and of many of its components, to gather
[Bootstrap](https://getbootstrap.com/) usage statistics.
It injects the collected data a JSON `<script>` in the page and this JSON string
is then read by the crawl script.

## License
Licensed under the MIT license (see LICENSE.txt)

Copyright (c) 2022 Inria
